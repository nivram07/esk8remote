#ifndef _BITMAPS_H_
#define _BITMAPS_H_

#include "ssd1306_hal/io.h"
#include <stdint.h>

extern const PROGMEM uint8_t heartImage[8];
extern const PROGMEM uint8_t fck[8];

#endif
